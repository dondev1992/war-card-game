/**
 * @assignment - Logic Layer War Exercise v2
 * @class - CO_CP Alpha 1
 * @author - Don Moore
 * @task - Recreate "I Declare War" game using Java classes, objects, and methods
 *         Get players names as input and use names to reference players
 *         Create deck, flip cards, collect books, and shuffle cards
 *         Display results of each round
 *         Display results of "going to war"
 */

import java.util.ArrayList;
import java.util.Collections;

public class Deck {
    private ArrayList<Card> cardDeck;
    private final ArrayList<String> cardSuits;
    private final ArrayList<String> cardRanks;
    public Deck(ArrayList<String> cardRanks, ArrayList<String> cardSuits, ArrayList<Card> cardDeck) {
        this.cardRanks = cardRanks;
        this.cardSuits = cardSuits;
        this.cardDeck = cardDeck;

        // loop through suit array
        for (int i = 0; i < 4; i++) {
            //assign suit to a varible
            String suit = cardSuits.get(i);
            //loop through ranks
            for (int j = 0; j < 13; j++) {
                // create card and assign rank and suit variable
                String rank = cardRanks.get(j);
                Card aCard = new Card(rank, suit);
                // add card to deck arraylist
                this.cardDeck.add(aCard);
            }
        }
    }
    public void dealCards(ArrayList<Card> deck, ArrayList<Card> dealCards1, ArrayList<Card> dealCards2) {
        for (int i = 0; i < deck.size(); i += 2) {
            Card currentCard1 = deck.get(i);
            dealCards1.add(currentCard1);
        }
        for (int i = 1; i < deck.size(); i += 2) {
            Card currentCard2 = deck.get(i);
            dealCards2.add(currentCard2);
        }
    }
}

