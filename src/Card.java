/**
 * @assignment - Logic Layer War Exercise v2
 * @class - CO_CP Alpha 1
 * @author - Don Moore
 * @task - Recreate "I Declare War" game using Java classes, objects, and methods
 *         Get players names as input and use names to reference players
 *         Create deck, flip cards, collect books, and shuffle cards
 *         Display results of each round
 *         Display results of "going to war"
 */

public class Card {
    private String cardRank;
    private String cardSuit;

    public Card(String cardRank, String cardSuit) {
        this.cardRank = cardRank;
        this.cardSuit = cardSuit;
    }

    public String getCardRank() {
        return cardRank;
    }

    public String getCardSuit() {
        return cardSuit;
    }

    public void displayCard() {
        System.out.println("Card: " + cardRank + " of " + cardSuit);
    }
}
