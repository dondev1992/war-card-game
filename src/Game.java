/**
 * @assignment - Logic Layer War Exercise v2
 * @class - CO_CP Alpha 1
 * @author - Don Moore and Jazmarie Monrief
 * @task - Recreate "I Declare War" game using Java classes, objects, and methods
 *         Get players names as input and use names to reference players
 *         Create deck, flip cards, collect books, and shuffle cards
 *         Display results of each round
 *         Display results of "going to war"
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Game {
    //

    /**
     * This method adds winning card hand to working hand if it is empty
     * @param workingHand1 - player one arraylist for competing in rounds
     * @param booksWonHand1 - player one arraylist for cards won
     * @param workingHand2 - player two arraylist for competing rounds
     * @param booksWonHand2 - player two arraylist for cards won
     */
    public static void addWinningBooksToWorkingHand(ArrayList<Card> workingHand1, ArrayList<Card> booksWonHand1, ArrayList<Card> workingHand2, ArrayList<Card> booksWonHand2) {
        if (!booksWonHand1.isEmpty()) {
            shuffleDeck(booksWonHand1);
            workingHand1.addAll(booksWonHand1);
            booksWonHand1.clear();
        }
        if (!booksWonHand2.isEmpty()) {
            shuffleDeck(booksWonHand2);
            workingHand2.addAll(booksWonHand2);
            booksWonHand2.clear();
        }
    }

    /**
     * This method is for displaying "I de- clare"
     */
    public static void declareStatement() {
        System.out.println("I");
        System.out.println("DE-");
        System.out.println("CLARE\n");
    }

    /**
     * This method declares war and displays info about who won the "I declare war" draw
     * @param winner - holds the name of the winner
     * @param playerName1 - holds the name of Player one
     * @param playerName2 - holds the name of Player two
     * @param card1 - card flipped from Player one
     * @param card2 - card flipped from Player two
     */
    public static void warStatement(String winner, String playerName1, String playerName2, Card card1, Card card2) {
        System.out.println("WAR WAR WAR!!!\n");
        System.out.println(playerName1 + "'s War Card: " + card1.getCardRank() + " of " + card1.getCardSuit());
        System.out.println(playerName2 + "'s War Card: " + card2.getCardRank() + " of " + card2.getCardSuit());
        System.out.println("\n" + winner + " wins the Standoff!!!");

    }

    /**
     * This method displays info about the current round of I of the game
     * @param winner - holds the name of the winner
     * @param playerName1 - holds the name of Player one
     * @param playerName2 - holds the name of Player two
     * @param card1 - card flipped from Player one
     * @param card2 - card flipped from Player two
     * @param round - holds count of the rounds
     */
    public static void showCurrentRound(String winner, String playerName1, String playerName2, int round, Card card1, Card card2) {
        System.out.println("Battle #" + round + " of the War");
        System.out.println("---------------------");
        System.out.println(playerName1 + ": " + card1.getCardRank() + " of " + card1.getCardSuit());
        System.out.println(playerName2 + ": " + card2.getCardRank() + " of " + card2.getCardSuit());
        System.out.println(winner + " won this Battle!");
    }
    public static void printGameWinner(String winner) {
        System.out.println(winner + " has won the WAR!");
    }

    /**
     * Determines the game winner by who has the most cards left over
     * @param player1WorkingHand - player one arraylist for competing in rounds
     * @param player2WorkingHand - player two arraylist for competing rounds
     * @param player1BooksWonHand - player one arraylist for cards won
     * @param player2BooksWonHand - player two arraylist for cards won
     * @param playerName1 - holds the name of Player one
     * @param playerName2 - holds the name of Player two
     */
    public static void chooseGameWinner(ArrayList<Card> player1WorkingHand, ArrayList<Card> player2WorkingHand, ArrayList<Card> player1BooksWonHand, ArrayList<Card> player2BooksWonHand, String playerName1, String playerName2) {
        int totalCardAmount1 = player1WorkingHand.size() + player1BooksWonHand.size();
        int totalCardAmount2 = player2WorkingHand.size() + player2BooksWonHand.size();

        if (totalCardAmount1 > totalCardAmount2) {
            printGameWinner(playerName1);
        } else if (totalCardAmount1 < totalCardAmount2) {
            printGameWinner(playerName2);
        } else {
            System.out.println(playerName1 + " and " + playerName2 + " have ended the War in a stalemate...");
            System.out.println("Game Over");
        }
    }
    public static void shuffleDeck(ArrayList<Card> deck) {
        Collections.shuffle(deck);
    }

    /**
     * This method starts the rounds of play and determines the winner of each round
     * @param player1 - the hand object for Player one
     * @param player2 - the hand object for Player two
     * @param rank - the arraylist of the card rank to use as a reference for comparing rank values between flipped cards
     */
    public static void playGame(Hand player1, Hand player2, ArrayList<String> rank) {
        // Creating variables for working hands and hands for books won
        ArrayList<Card> player1WorkingHand = player1.getWorkingHand();
        ArrayList<Card> player2WorkingHand = player2.getWorkingHand();
        ArrayList<Card> player1BooksWonHand = player1.getBooksWonHand();
        ArrayList<Card> player2BooksWonHand = player2.getBooksWonHand();

        // Variables for the two cards being flipped
        Card flipCard1;
        Card flipCard2;

        // Variables to hold the original indices of the flipped cards to use for comparing values
        int flipCard1Index;
        int flipCard2Index;

        int round = 1;

        String playerName1 = player1.getPlayerName();
        String playerName2 = player2.getPlayerName();
        String winner = "";

        // While loop for comparing the indices of the two flipped cards, determining the winner, then adding the cards to the winner's won books hand
        while (!player1WorkingHand.isEmpty() || !player2WorkingHand.isEmpty()) {
            // If working hand is empty, add cards to working hand from won books hand
            if (player1WorkingHand.size() == 0 || player2WorkingHand.size() == 0) {
                addWinningBooksToWorkingHand(player1WorkingHand,player1BooksWonHand, player2WorkingHand, player2BooksWonHand);
            }
            // Get flipped cards from top of list
            flipCard1 = player1WorkingHand.remove(player1WorkingHand.size() - 1);
            flipCard2 = player2WorkingHand.remove(player2WorkingHand.size() - 1);

            // Use card rank to find index of card from array of card rank
            flipCard1Index = rank.indexOf(flipCard1.getCardRank());
            flipCard2Index = rank.indexOf(flipCard2.getCardRank());

            if (player1WorkingHand.size() == 0 || player2WorkingHand.size() == 0) {
                // If working hand is empty, add cards to working hand from won books hand
                addWinningBooksToWorkingHand(player1WorkingHand,player1BooksWonHand, player2WorkingHand, player2BooksWonHand);
            }
            // compare index to determine card value hierarchy for winner of current round
            if (flipCard1Index > flipCard2Index) {
                player1BooksWonHand.add(flipCard1);
                player1BooksWonHand.add(flipCard2);
                winner = playerName1;
                showCurrentRound(winner, playerName1, playerName2, round, flipCard1, flipCard2);
                round++;

            } else if (flipCard1Index < flipCard2Index) {
                player2BooksWonHand.add(flipCard1);
                player2BooksWonHand.add(flipCard2);
                winner = playerName2;
                showCurrentRound(winner, playerName1, playerName2, round, flipCard1, flipCard2);
                round++;

            } else {
                System.out.println(playerName1 + ": " + flipCard1.getCardRank() + " of " + flipCard1.getCardSuit());
                System.out.println(playerName2 + ": " + flipCard2.getCardRank() + " of " + flipCard2.getCardSuit() + "\n");
                // If both card rank values are equal, call the declareWar method
                if (player1WorkingHand.size() < 4 || player2WorkingHand.size() < 4) {
                    break;
                } else {
                    declareWar(player1WorkingHand, player2WorkingHand, player1BooksWonHand, player2BooksWonHand, rank, playerName1, playerName2);
                    round++;
                }
            }
            System.out.println("---------------------------\n");
//            System.out.println("Working Hand 1: " + player1WorkingHand.size());
//            System.out.println("Working Hand 2: " + player2WorkingHand.size());
//            System.out.println("Books Hand 1: " + player1BooksWonHand.size());
//            System.out.println("Books Hand 2: " + player2BooksWonHand.size());
        }

        // When while loop terminates, call chooseGameWinner method
        chooseGameWinner(player1WorkingHand, player2WorkingHand, player1BooksWonHand, player2BooksWonHand, playerName1, playerName2);
    }

    /**
     * This method runs the "I declare war" scenario where three cards are flipped per player first, the the fourth card is flipped per player and its value compared to determine the winner
     * @param player1WorkingHand - player one arraylist for competing in rounds
     * @param player2WorkingHand - player two arraylist for competing rounds
     * @param player1BooksWonHand - player one arraylist for cards won
     * @param player2BooksWonHand - player two arraylist for cards won
     * @param playerName1 - holds the name of Player one
     * @param playerName2 - holds the name of Player two
     * @param rank - the arraylist of the card rank to use as a reference for comparing rank values between flipped cards
     */
    private static void declareWar(ArrayList<Card> player1WorkingHand, ArrayList<Card> player2WorkingHand, ArrayList<Card> player1BooksWonHand, ArrayList<Card> player2BooksWonHand, ArrayList<String> rank, String playerName1, String playerName2) {
        System.out.println("<<<<<<<<<<Start War>>>>>>>>>>");
        // Create new arraylist to hold the cards drawn for declaring war
        ArrayList<Card> warCardPile = new ArrayList<>();
        Card drawCard1;
        Card drawCard2;
        String winner = "";

        // Store starting array length in variables for 'for loop'
        int workingHandLength1 = player1WorkingHand.size();
        int workingHandLength2 = player2WorkingHand.size();
        // Call "I declare" statement method
        declareStatement();
        System.out.println(playerName1 + "'s I De-Clare Cards:");
        //  Pull 3 cards from working hand of player 1
        for (int i = (workingHandLength1 - 1); i > (workingHandLength1 - 4); i--) {
            drawCard1 = player1WorkingHand.remove(i);
            warCardPile.add(drawCard1);
            System.out.println(drawCard1.getCardRank() + " of " + drawCard1.getCardSuit());
        }
        System.out.println("--------------");
        // Call "I declare" statement method
        declareStatement();
        System.out.println(playerName2  + "'s I De-Clare Cards:");
        //  Pull 3 cards from working hand of player 2
        for (int i = (workingHandLength2 - 1); i > (workingHandLength2 - 4); i--) {
            drawCard2 = player2WorkingHand.remove(i);
            warCardPile.add(drawCard2);
            System.out.println(drawCard2.getCardRank() + " of " + drawCard2.getCardSuit());
        }
        System.out.println("--------------");

        // Store 4th flipped card in variables
        Card flipLastCard1 = player1WorkingHand.remove(player1WorkingHand.size() - 1);
        Card flipLastCard2 = player2WorkingHand.remove(player2WorkingHand.size() - 1);
        // Get indices for 4th cards from rank array
        int flipLastCard1Index = rank.indexOf(flipLastCard1.getCardRank());
        int flipLastCard2Index = rank.indexOf(flipLastCard2.getCardRank());

        // Compare indices to determine which card has the higher value
        if (flipLastCard1Index > flipLastCard2Index) {
            player1BooksWonHand.addAll(warCardPile);
            player1BooksWonHand.add(flipLastCard1);
            player1BooksWonHand.add(flipLastCard2);
            warCardPile.clear();
            winner = playerName1;
            warStatement(winner, playerName1, playerName2,flipLastCard1, flipLastCard2);
        } else {
            player2BooksWonHand.addAll(warCardPile);
            player2BooksWonHand.add(flipLastCard1);
            player2BooksWonHand.add(flipLastCard2);
            warCardPile.clear();
            winner = playerName2;
            warStatement(winner, playerName1, playerName2,flipLastCard1, flipLastCard2);
        }
        System.out.println("<<<<<<<<<<<End War>>>>>>>>>>>\n");
    }

    public static void main(String[] args) {

        Scanner inputField = new Scanner(System.in);

        System.out.print("Enter name of Player 1: ");
        String playerName1 = inputField.nextLine();

        System.out.print("Enter name of Player 1: ");
        String playerName2 = inputField.nextLine();


        System.out.println("<<<<<<<<<<<Start Game>>>>>>>>>>>\n\n");

        // Create constant values for card creation and put in arraylists
        final ArrayList<String> cardSuits = new ArrayList<>(Arrays.asList("Spades", "Diamonds", "clubs", "Hearts"));
        final ArrayList<String> cardRanks = new ArrayList<>(Arrays.asList("Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"));

        // Create empty arraylist for card deck
        ArrayList<Card> cardDeck = new ArrayList<>();

        // Create a card deck
        Deck deck = new Deck(cardRanks, cardSuits, cardDeck);

        // Clone deck then shuffle cards
        ArrayList<Card> shuffledDeck = (ArrayList<Card>)cardDeck.clone();
        shuffleDeck(shuffledDeck);

        // Create two arraylists to hold each dealt hand
        ArrayList<Card> dealtCards1 = new ArrayList<>();
        ArrayList<Card> dealtCards2 = new ArrayList<>();
        // Deal cards from deck into dealtCards1 and dealtCards2
        deck.dealCards(shuffledDeck, dealtCards1, dealtCards2);

        // Create hand objects
        Hand player1 = new Hand();
        Hand player2 = new Hand();

        // Add player names to hand objects
        player1.setPlayerName(playerName1);
        player2.setPlayerName(playerName2);

        // Add dealtCard arraylists to each hand
        player1.setWorkingHand(dealtCards1);
        player2.setWorkingHand(dealtCards2);

        // Call playGame method to start rounds
        playGame(player1, player2, cardRanks);

        System.out.println("\n\n<<<<<<<<<<<Game Over>>>>>>>>>>>");
    }
}