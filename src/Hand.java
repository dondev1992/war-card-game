/**
 * @assignment - Logic Layer War Exercise v2
 * @class - CO_CP Alpha 1
 * @author - Don Moore
 * @task - Recreate "I Declare War" game using Java classes, objects, and methods
 *         Get players names as input and use names to reference players
 *         Create deck, flip cards, collect books, and shuffle cards
 *         Display results of each round
 *         Display results of "going to war"
 */

import java.util.ArrayList;

public class Hand {
    private String playerName;
    private ArrayList<Card> workingHand;
    private ArrayList<Card> booksWonHand = new ArrayList<>();

    public Hand() {

    }

    public String getPlayerName() {
        return playerName;
    }

    public ArrayList<Card> getWorkingHand() {
        return workingHand;
    }

    public ArrayList<Card> getBooksWonHand() {
        return booksWonHand;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setBooksWonHand(ArrayList<Card> booksWonHand) {
        this.booksWonHand = booksWonHand;
    }

    public void setWorkingHand(ArrayList<Card> workingHand) {
        this.workingHand = workingHand;
    }
}
